# PJ- Analysis

This repository contains notebooks of the PJ data analysts team.

## Flight Overspend Analysis

## Campaign Config Dashboard

Flight configuration analysis, providing "red flags" definitions for bad configuration

Ticket - http://jira.tmp.com/browse/DSA-66

Dashboard link - https://datastudio.google.com/u/0/reporting/322693f9-a518-4474-ada7-c19d0a2f7ada/page/T3V2B


## CPA performance

CPA analysis since 01/2020 for job category (SOC-minor), job board, and the combination of both.

Ticket - http://jira.tmp.com/browse/DSA-56

Dashboard link - https://datastudio.google.com/u/0/reporting/9703f7d1-371f-4ad4-811c-61620abbd447/page/k3G5B

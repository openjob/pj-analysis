import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pandas as pd
from sqlalchemy import create_engine

# database connection
db = create_engine('postgres://root:Yf3Ujq94s92oAY0l@pgsqlrr.prod.perengo.com/perengo_v2')
conn = db.connect()

# get daily spend data
df = pd.read_sql('''
SELECT count(*) as count
FROM final_campaigns
WHERE status = 'assigned'
  AND "jobCampaignId" IN
    (SELECT id
     FROM job_campaigns
     WHERE "customerId" = 27588)
  AND job_board = 'jb_aaps'
''', conn)
print('ttt', df.iloc[0]['count'])

gmail_user = 'tomerlevradancy'
gmail_password = 'Qe13!id12asd'

#email properties
sent_from = gmail_user
email_receivers = ['tomer.lev@tmp.com']
subject = 'ALERT: No Live Genentech Jobs - jb_aaps'
email_text = """
There are currently %s assigned jobs for Genentech 
on jb_aaps in the Programmatic Jobs platform. 
This means no jobs are being sent to the publisher. 
Please investigate and escalate appropriately.
""" % (str(df.iloc[0]['count']))
#email send request
try:
    for to in email_receivers:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        message = MIMEMultipart('alternative')
        message['From'] = sent_from
        message['To'] = to
        message['Subject'] = subject
        message.attach(MIMEText(email_text, 'html'))
        text = message.as_string()
        server.sendmail(sent_from,to,text)
        server.close()
        print ('Email sent!')

except Exception as e:
    print(e)
    print ('Something went wrong...')